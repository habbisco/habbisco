<?php

/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 05/05/17
 * Time: 18:16
 */
class RegisterController extends Controller{

    // Ação Padrão
    public function index(){
        $this->method('GET', function(){
            $this->render('register');
        });

        $this->method('POST', function(){
            if(isset(
                $_POST['email'],
                $_POST['pwd'],
                $_POST['username'],
                $_POST['gender'],
                $_POST['birth-day'],
                $_POST['birth-month'],
                $_POST['birth-year'],
                $_POST['terms']
            ) && sizeof($_POST['terms']) == 1){

                $day   = (int) $_POST['birth-day'];
                $month = (int) $_POST['birth-month'];
                $year  = (int) $_POST['birth-year'];

                $day   = $day   < 10 ? "0$day"   : $day  ;
                $month = $month < 10 ? "0$month" : $month;

                $birthday = "$year-$month-$day";

                $user = new UserModel([
                    'username'  => $_POST['username'],
                    'mail'      => $_POST['email'],
                    'gender'    => $_POST['gender'],
                    'password'  => $_POST['pwd'],
                    'birthday'  => $birthday
                ]);

                if($user->save()){
                    $this->redirect('/me', ['user' => $user]);
                } else {
                    $this->render('register', [
                        'error' => 'Ops! Não pudemos registrar o seu avatar. Tente novamente'
                    ]);
                }
            } else {
                $this->redirect('/register');
            }
        });

    }

    public function check_user()
    {
        $this->method('GET', function(){
            $this->redirect('/register');
        });

        $this->method('POST', function(){
            $stmt = Fn::$db->prepare("SELECT IF(
                                                      username = ':username', 'username', 
                                                      IF(mail = ':mail', 'mail', 0)
                                                    ) as user_exists 
                                                    FROM `users` 
                                                    WHERE username = ':username' OR mail = ':mail'
                                                    ");
            $stmt->bindParam('username', $_POST['username']);
            $stmt->bindParam('mail', $_POST['email']);
            $stmt->execute();
            $field = $stmt->fetch(PDO::FETCH_OBJ)->f;

            $this->json([
                'exists' => $field == '' ? false : $field
            ]);
        });
    }
}