<?php

/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 05/05/17
 * Time: 13:40
 */
class IndexController extends Controller{
    public function index(){
        $this->render('index', [
            'user' => (object) [
                'name' => 'Maria'
            ]
        ]);
    }
}