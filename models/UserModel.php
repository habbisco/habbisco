<?php

/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 05/05/17
 * Time: 18:51
 */
class UserModel extends Model{
    const TABLE = 'users';

    /**
     * User constructor
     * @param $data
     */
    public function __construct($data)
    {
        parent::__construct(self::TABLE, 'id', $data);
    }

    /**
     * @return object|string
     */
    public function generateAuthTicket()
    {
        $now = new DateTime();
        $length = random_int(4, 8);
        $bytes = openssl_random_pseudo_bytes($length);
        $token = $now->getTimestamp() . $bytes;
        $token = Secure::hash($token);

        $this->auth_ticket = $token;
        $this->update();

        return $token;
    }
}

// Antes de salvar o usuário, crie um hash e um salt para ele
UserModel::pre('save', function ($user) {
    if (!$user->password) return;

    $hash = Secure::hash($user->password);

    $user->password = $hash->hash;
    $user->salt = $hash->salt;
});