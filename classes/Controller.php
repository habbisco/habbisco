<?php

define("HTTP_ERRORS", [
    '401' => [
        'message' => 'Não Autorizado',
        'body'    => 'Falha na autenticação.'
    ],
    '403' => [
        'message' => 'Acesso Proibido',
        'body'    => 'Falha na autenticação.'
    ],
    '404' => [
        'message' => 'Não Encontrado',
        'body'    => 'A página que você está procurando não existe ou foi movida.'
    ],
    '500' => [
        'message' => 'Erro Interno do Servidor',
        'body'    => 'O servidor encontrou algum problema que impediu atender a sua requisição.'
    ],
    '502' => [
        'message' => 'Gateway Ruim',
        'body'    => 'O servidor encontrou algum problema que impediu atender a sua requisição.'
    ]
]);

/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 05/05/17
 * Time: 13:24
 */
abstract class Controller{
    protected $args;
    protected $url;
    private $name;

    /**
     * Controller constructor.
     * @param $name
     * @param $url
     */
    public function __construct($name, $args, $url){
        $this->name = $name;
        $this->args = $args;
        $this->url  = $url;
    }

    /**
     * Renderiza uma view
     * @param $viewname
     * @param array $data
     */
    public function render($viewname, $data = []){
        $view = new View($viewname);
        View::bootstrap();
        $view->render($data);
    }

    /**
     * Default action
     */
    public function index(){
        View::renderGeneric('index', [ 'controller' => $this->name ]);
    }

    /**
     * Executa o callback de acordo com o tipo de requisição
     * @param $method
     * @param $callback
     * @return null
     */
    public function method($method, $callback){
        $method = strtoupper($method);
        if($_SERVER['REQUEST_METHOD'] === $method) return $callback();
        return null;
    }

    /**
     * Exibe um erro
     * @param int $status
     */
    public function error($status=500, $namespace = 'http', $message = null, $body = null){
        $error = HTTP_ERRORS[$status] ?? HTTP_ERRORS[500];
        $message = $message ?? $error['message'];
        $body = $body ?? $error['body'];
        $error = new HttpError($message, $body, $status);

        $error->sendStatus();

        View::render_generic($namespace.'.error', [
            'error'     => $error,
            'namespace' => $namespace
        ]);
    }

    /**
     * Define o content Type
     * @param $mimetype
     */
    public function mimeType($mimetype){
        $mimetype = is_array($mimetype) ? join(";", $mimetype) : (string) $mimetype;
        header("Content-Type: $mimetype;");
    }

    public function statusCode($code){
        http_response_code($code);
    }

    /**
     * Redireciona a página para uma url
     * @param $url
     * @param int $status
     * @return $this
     */
    public function redirect($url, $status=302){
        $url = preg_match(":^\w+\:\/\/:", $url) ? $url : ABS_PATH.$url;

        $this->statusCode($status);
        header('Location: '.$url);

        return $this;
    }

    /**
     * Envia dados como JSON
     * @param $object
     */
    public function json($object){
        $this->mimeType(['application/json', 'text/javascript']);
        echo json_encode($object);
    }
}