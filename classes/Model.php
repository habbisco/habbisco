<?php
/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 05/05/17
 * Time: 19:08
 */

abstract class Model{
    protected $__table__;
    protected $__fields__;
    protected $__pk__;
    private static $__pre__ = [];

    /**
     * Model constructor.
     * @param $table
     * @param $pk
     * @param $data
     * @return Model
     */
    public function __construct($table, $pk, $data){
        $this->__table__  = $table;
        $this->__pk__     = $pk;
        $this->__fields__ = array_keys($data);

        $this->construct($data);
    }

    /**
     * Salva o model no banco de dados
     */
    public function save(){
        self::__run_pre__(__FUNCTION__, $this);

        $data = $this->__getData();
        return Fn::$db->insert($this->__table__, array_keys($data), $data);
    }

    /**
     * Atualiza os dados do model no banco de dados
     */
    public function update(){
        self::__run_pre__(__FUNCTION__, $this);
        $data = $this->__getData();

        $stmt = "UPDATE `{$this->__table__}` SET `".join("`=?,`", $this->__fields__)."`=? WHERE `{$this->__pk__}`=?";
        $params = array_values(array_merge($data, [$data[$this->__pk__]]));

        $stmt = Fn::$db->pquery($stmt, $params);

        return $stmt->errorCode() == '00000';
    }

    /**
     * Apaga o model do banco de dados
     */
    public function delete(){
        self::__run_pre__(__FUNCTION__, $this);

        $stmt = "DELETE FROM `{$this->__table__}` WHERE `{$this->__pk__}`=?";
        $stmt = Fn::$db->pquery($stmt, $this->__data__[$this->__pk__]);

        return $stmt->errorCode() == '00000';
    }

    /**
     * Atualiza o cache do objeto
     */
    public function read(){
        self::__run_pre__(__FUNCTION__, $this);

        $data = $this->__getData();

        $stmt = "SELECT * FROM `{$this->__table__}` WHERE `".join('`=? AND `', array_keys($data))."`=? LIMIT 1";
        $stmt = Fn::$db->pquery($stmt, $data);
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->destroy();
        $this->construct($data);

        return $data;
    }

    /**
     * Define dados da instância
     * @param $data
     */
    public function construct($data){
        $data = (array) $data;
        $this->__fields__ = array_keys($data);

        foreach ($data as $property => $value){
            $this->$property = $value;
        }
    }

    /**
     * Apaga a instância do model
     */
    public function destroy(){
        self::__run_pre__(__FUNCTION__, $this);

        // Ignorar propriedadas da própria classe
        $ignore       = array_merge(get_class_vars(self::class));
        $properties   = get_object_vars($this);
        $properties   = array_keys($properties);

        foreach($properties as $property){
            if(!in_array($property, $ignore)){
                unset($this->$property);
            }
        }

        return $this;
    }

    /**
     * Define uma função a ser chamada antes de executar uma ação
     * @param $action
     * @param $callback
     */
    public static function pre($action, $callback){
        self::$__pre__[$action] = $callback;
    }

    /**
     * Executa uma definida função antes de executar uma ação
     * @param $action
     * @param $instance
     */
    public static function __run_pre__($action, &$instance){
        $function = self::$__pre__[$action] ?? function(){};
        $function($instance);
    }

    /**
     * Retorna os dados do model
     * @return array
     */
    private function __getData(){
        $fields = array_keys(get_object_vars($this));
        $data = [];

        foreach ($fields as $field){
            if(preg_match(":__.+__:", $field)) continue;

            $data[$field] = $this->$field;
        }

        return $data;
    }
}