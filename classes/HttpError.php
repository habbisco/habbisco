<?php

/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 07/05/17
 * Time: 12:35
 */
class HttpError{
    public $message;
    public $body;
    public $status;
    /**
     * HttpError constructor.
     * @param $message
     * @param string $body
     * @param int $code
     */
    public function __construct($message, $body='', $code=500){
        $code = (int) $code;
        $code = max(400, $code);
        $code = min(599, $code);
        $this->message = $message;
        $this->body = $body;
        $this->status = $code;
    }

    /**
     * Envia o status para o navegador
     */
    public function sendStatus(){
        http_response_code($this->status);
    }
}