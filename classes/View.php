<?php

/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 05/05/17
 * Time: 11:53
 */
class View{
    private $name;
    private $vars = [];
    private $path;
    private static $theme_path;
    private static $layout = '__layout__';
    private static $global_vars = ['title'=>'View sem título'];
    private static $generic_theme;
    private static $bootstrap = '__theme__.php';
    private static $partials = 'partials';

    /**
     * View constructor.
     * @param string $name
     * @param string|null $base
     * @param array $vars
     */
    public function __construct(string $name, string $base = null, array $vars = []){
        $this->name = $name;
        $this->path = $base ?? self::$theme_path ?? self::$generic_theme;
        $this->path .= '/'.$name.'.view.php';
        $this->vars = $vars;
    }

    public static function setGlobals(array $vars)
    {
        self::$global_vars = $vars;
    }

    /**
     * Define o arquivo de layout
     * @param string $viewname
     */
    public static function setLayout(string $viewname){
        self::$layout = $viewname;
    }

    /**
     * Retorna se uma view existe
     * @param $view
     * @return bool
     */
    public static function exists(string $view){
        return file_exists(self::$theme_path.'/'.$view.'.view.php');
    }

    /**
     * Define o tema genérico
     * @param $dir
     */
    public static function setGenericTheme(string $dir){
        self::$generic_theme = $dir;
    }

    /**
     * Define o tema das views
     * @param $dir
     */
    public static function setTheme(string $dir){
        self::$theme_path = $dir;
    }

    /**
     * Exibe um erro
     * @param string $message
     * @param bool $with_view
     */
    public static function error(string $message, bool $with_view = true){
        if(!($with_view && self::render_generic('view.error', ['message' => $message]))){
            echo "<h1>O View encontrou o problema: </h1>";
            echo "<p>{$message}</p>";
        }
    }

    /**
     * Renderiza a view
     * @param array $vars
     * @param bool $layout
     * @return bool
     */
    public function render(array $vars = [], bool $layout = true){
        $vars = array_merge(self::$global_vars, $this->vars, $vars);
        $layout_file = self::$theme_path.'/'.self::$layout.'.view.php';

        if($layout && View::exists(self::$layout)){
            extract($vars);
            include $layout_file;
        } else {
            if(View::exists($this->name)){
                $this->content_view($vars);
            } else {
                self::error("<strong>{$this->path}</strong> não existe.");
                return true;
            }
        }
        return true;
    }

    /**
     * Obtem o conteúdo da view
     * @param array $vars
     */
    private function content_view(array $vars = []){
        if(self::exists($this->name)){
            extract($vars);
            include $this->path;
        } else {
            self::error("Falha na renderização de '{$this->name}', o arquivo não existe.");
        }
    }

    /**
     * Define o arquivo de Boostrap do tema
     * @param $file
     */
    public static function setBootstrap(string $file){
        self::$bootstrap = $file;
    }

    /**
     * Inicia as configurações tema por meio do arquivo de bootstrap
     */
    public static function bootstrap(){
        $bootstrap = self::$theme_path.'/'.self::$bootstrap;
        if(file_exists($bootstrap)) include_once $bootstrap;
    }

    /**
     * Retorna o caminho absoluto do tema
     */
    public function theme(){
        $path = Fn::relativePath($_SERVER['DOCUMENT_ROOT'], VW_THEME);
        $path = preg_replace(':\\\:', "/", $path);
        return $path;
    }

    /**
     * Define a pasta de parciais
     * @param $dir
     */
    public static function setPartials(string $dir){
        self::$partials = trim($dir, '\\/');
    }

    /**
     * Carrega uma parcial
     * @param string $name
     * @param array $vars
     */
    public function partial(string $name, array $vars = []){
        $partial = self::$theme_path.'/'.self::$partials.'/'.$name.'.partial.php';

        if(file_exists($partial)){
            extract($vars);
            include $partial;
        } else {
            self::error("A parcial '{$name}' não existe.");
        }
    }

    /**
     * Renderiza uma view genérica
     * @param array $vars
     * @param bool $layout
     * @return bool
     */
    public static function render_generic(string $name, array $vars = [], bool $layout = true){
        $vars = array_merge(self::$global_vars, $vars);
        $view = self::$generic_theme.'/'.$name.'.php';

        if(file_exists($view)){
            extract($vars);
            include $view;
            return true;
        }
        return false;
    }
}