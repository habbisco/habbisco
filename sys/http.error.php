<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Erro <?=$error->status; ?>: <?=$error->message; ?></title>
</head>
<body>
<section id="E">
    <h1>Erro <?=$error->status; ?></h1>
    <h2><?=$error->message; ?></h2>
    <section class="body">
        <?=$error->body; ?>
    </section>
</section>
</body>
</html>