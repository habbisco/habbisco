<?php $this->partial('header'); ?>

<?php if(isset($error)){ ?>
    <div class="error"><?=$error?></div>
<?php }; ?>

    <!-- Site Wrapper -->
    <div class="site-wrapper gs-row">
        <div class="left col-4">
            <h1>ENTRE AGORA!</h1>

            <!-- Register form -->
            <form id="register-form" class="reg-form col-12" method="post" action="<?= ABS_PATH ?>/register">
                <div class="input-group-box">
                    <div class="input-group">
                        <div class="input-label">
                            <label for="username">Nome de Usuário</label>
                            <p>
                                Este será o seu login e também o seu nome no Habbisco Hotel.
                                Outros jogadores poderão te adicionar como amigo usando esta informação.
                            </p>
                        </div>
                        <input type="text" name="username" id="username" class="form-control" placeholder="Nome de Usuário" required>
                    </div>
                    <div class="input-group">
                        <div class="input-label">
                            <label>Gênero</label>
                            <p>Você é...</p>
                            <div class="center">
                                <div class="input-radio gender">
                                    <input type="radio" name="gender" id="gender_male" value="m" required>
                                    <label for="gender_male">
                                        <img src="<?= $this->theme(); ?>/_assets/img/male_sign.png" alt="Homem">
                                        <span>Homem</span>
                                    </label>
                                </div>
                                <div class="input-radio gender">
                                    <input type="radio" name="gender" id="gender_female" value="f" required>
                                    <label for="gender_female">
                                        <img src="<?= $this->theme(); ?>/_assets/img/female_sign.png" alt="Mulher">
                                        <span>Mulher</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- E-mail -->
                <div class="input-group">
                    <div class="input-label">
                        <label for="email">E-mail</label>
                        <p>Você vai precisar deste endereço de e-mail para realizar o login no Habbisco Hotel ou recuperar a sua conta. Por favor, utilize um endereço de e-mail válido.</p>
                    </div>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required>
                </div>
                <!-- Password -->
                <div class="input-group-box">
                    <div class="input-group">
                        <div class="input-label">
                            <label for="pwd">Senha</label>
                            <p>Utilize, pelo menos, 6 caracteres. Inclua, pelo menos, uma letra, um número e um caracter especial.</p>
                        </div>
                        <input type="password" name="pwd" id="pwd" minlength="6" class="form-control" placeholder="Senha" required data-equalto="#pwd2">
                    </div>
                    <div class="input-group">
                        <div class="input-label">
                            <label for="pwd2">Repita a Senha</label>
                        </div>
                        <input type="password" name="pwd2" id="pwd2" class="form-control" placeholder="Repita a Senha" required>
                    </div>
                </div>
                <!-- Birth Date -->
                <div class="input-group">
                    <div class="input-label">
                        <label for="birth-day">Data de Nascimento</label>
                        <p>Por favor, preencha com a sua data de nascimento REAL. Essa informação é muito importante caso você necessite recuperar a sua conta ou desbloqueá-la. Essa informação não será visível publicamente.</p>
                    </div>
                    <div class="input-group inline">
                        <select class="form-control" name="birth-day" id="birth-day" required>
                            <option value disabled selected hidden>Dia</option>
                            <?php
                            for($i = 1; $i <= 31; $i++) {
                                echo "<option value='$i'>$i</option>";
                            }
                            ?>
                        </select>
                        <select class="form-control" name="birth-month" id="birth-month" required>
                            <option value disabled selected hidden>Mês</option>
                            <?php
                            foreach(['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                                        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'] as $id => $name) {
                                echo "<option value='".($id+1)."'>$name</option>";
                            }
                            ?>
                        </select>
                        <select class="form-control" name="birth-year" id="birth-year" required>
                            <option value disabled selected hidden>Ano</option>
                            <?php
                            for($i = 2009; $i > 1900; $i--) {
                                echo "<option value='$i'>$i</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <!-- Terms and Conditions -->
                <div class="input-group-box">
                    <div class="input-group inline">
                        <input type="checkbox" name="terms" id="terms" required value="accepted">
                        <label for="terms">Eu aceito os <a href="#terms">Termos e Condições, Política de Privacidade e Política de Cookies</a>.</label>
                    </div>
                </div>
                <!-- Submit Button -->
                <button type="submit" class="btn-success large">PRONTO! CRIE SEU AVATAR!</button>
            </form>
        </div>
        <div class="right col-8">
            <img src="<?= $this->theme(); ?>/_assets/img/register.png">
        </div>
    </div>
<?php $this->partial('footer'); ?>