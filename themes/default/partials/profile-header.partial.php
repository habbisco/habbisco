<!-- Header -->
<header id="masthead" class="profile-header">
    <div class="content">
        <a href="<?= ABS_PATH ?>" rel="home" class="header-brand particle-squares">
            <img src="<?= $this->theme(); ?>/_assets/img/logo.gif" alt="Habbisco" />
        </a>
        <a href="<?= ABS_PATH ?>/login" class="btn-primary login-btn">ENTRAR</a>
    </div>
    <div class="navbar">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a href="#">Meu Perfil</a>
            </li>
            <li class="nav-item">
                <a href="#">Comunidade</a>
            </li>
            <li class="nav-item">
                <a href="#">Suporte</a>
            </li>
        </ul>
    </div>
</header>