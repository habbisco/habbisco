<!-- Header -->
<header id="masthead" class="header">
    <div class="content">
        <a href="<?= ABS_PATH ?>" rel="home" class="header-brand particle-squares">
            <img src="<?=$this->theme();?>/_assets/img/logo.gif" alt="Habbisco" />
        </a>
        <form class="form-inline login-form" method="post" action="<?= ABS_PATH ?>/login">
            <div class="input-group">
                <input class="form-control" type="email" spellcheck="false" placeholder="Email" required data-messages='{"empty":"Informe seu endereço de email."}'>
            </div>
            <div class="input-group">
                <input class="form-control" type="password" placeholder="Senha" required data-messages='{"empty":"Informe sua senha para entrar."}'>
                <label><a href="#">Esqueci minha senha.</a></label>
            </div>
            <div class="input-groups">
                <button class="btn-primary" type="submit">Login</button>
            </div>
        </form>
    </div>
</header>