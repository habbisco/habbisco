$(document).ready(function () {

    var $form = $('#status-form'),
        $status     = $('#me-status'),
        status      = $status.innerText || "",
        $edit       = $('#edit-status');

    $edit.click(function (e) {
        e.preventDefault();

        if($status.attr('readonly')) {
            $edit.children().removeClass('hbbi-tools-edit').addClass('hbbi-tools-save');
            $status.removeAttr('readonly').removeClass('readonly').focus();
        } else {
            $edit.children().removeClass('hbbi-tools-save').addClass('hbbi-tools-edit');
            $status.attr('readonly', '').addClass('readonly');
            $form.submit();
        }
    });
    $status.keypress(function (e) {
        if(e.keyCode == 13 && !$status.attr('readonly')) {
            e.preventDefault();
            $edit.click();
        }
    })
});