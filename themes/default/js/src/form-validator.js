///////////////////////
//  Form Validation  //
///////////////////////

$(document).ready(function () {

    $('#pwd2').keyup(function () {
        var pwd = $('#pwd');

        if(this.value !== pwd.val()) {
            this.setCustomValidity("As senhas não correspondem.");
        } else {
            this.setCustomValidity("");
        }
    });

});