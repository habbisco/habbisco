<?php
/**
 * Created by PhpStorm.
 * User: adrian
 * Date: 29/04/2017
 * Time: 16:44
 */

require_once "_assets/inc/core.php";
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>Habbisco Hotel - Jogue agora!</title>

    <!-- Habbisco Stylesheet -->
    <link rel="stylesheet" href="<?=$this->theme();?>/_assets/css/habbisco.css">
    <!-- Material Design Icons -->
    <link rel="stylesheet" href="<?=$this->theme();?>/bower_components/mdi/css/materialdesignicons.css">
</head>
<body class="<?= $this->name?>">
<div id="site">
    <?php $this->theme(); ?>
    <?php $this->content_view($vars); ?>
</div>
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="<?=$this->theme();?>/_assets/js/jquery-3.2.1.min.js"></script>

<!-- Habbisco JS -->
<script src="<?=$this->theme();?>/js/min/habbisco.min.js"></script>
</body>
</html>