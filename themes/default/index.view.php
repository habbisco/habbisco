<?php $this->partial('header') ?>

<!-- Site Wrapper -->
<div class="site-wrapper gs-row">
    <!-- News -->
    <section class="news gs-row">

        <div class="featured-img" style="background-image: url(<?=$this->theme()?>/_assets/img/news.png);"></div>
        <div class="featured-content">
            <h3>Title of Lorem Ipsum</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Animi dolor doloribus id, ipsam odit quod recusandae repudiandae sunt.</p>
            <a class="more" href="#">Ler mais</a>
        </div>

    </section>

    <!-- Play Now -->
    <div class="play-now">
        <h1>JOGUE AGORA MESMO!<br>É <span class="flasher particle-confetti">GRÁTIS</span>!</h1>
        <a href="<?php echo ABS_PATH?>/register" class="btn-play btn-success large">
            CADASTRE-SE!
            <small>100% grátis.</small>
        </a>
    </div>
</div>

<?php $this->partial('footer') ?>
