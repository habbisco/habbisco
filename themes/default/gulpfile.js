var gulp   = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    watch  = require('gulp-watch');

//script paths
var src      = 'js/src/**/*.js',
    dest       = 'js/',
    output     = 'habbisco.js',
    dest_min   = 'js/min/',
    output_min = 'habbisco.min.js';

gulp.watch(src, ['scripts']);

gulp.task('scripts', function() {
    return gulp.src(src)
        .pipe(concat(output))
        .pipe(gulp.dest(dest))
        .pipe(rename(output_min))
        .pipe(uglify())
        .pipe(gulp.dest(dest_min));
});

gulp.task('default', ['scripts']);