<?php

$__HOME = "/habbisco";

// Functions

// Link
function wlink($url) {
    global $__HOME;
    echo $__HOME . '/' . $url;
}

// Elements
function get_element($el) {
    $base_uri = "_assets/inc/elements/";

    if(is_array($el)) {
        foreach ($el as &$name) {
            $file = $base_uri.$name.".php";
            if(is_file($file)) {
                include $file;
            }
        }
    } else {
        $file = $base_uri.$el.".php";
        if(is_file($file)) {
            include $file;
        }
    }
}