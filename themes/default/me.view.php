<?php $this->partial('profile-header'); ?>

    <!-- Site Wrapper -->
    <div class="site-wrapper gs-row">
        <div class="container">
            <!-- Navbar -->
            <nav class="navbar">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#">Configurações</a>
                    </li>
                    <li class="nav-item">
                        <a href="#">Facebook</a>
                    </li>
                </ul>
            </nav>

            <!-- Player Info -->
            <section class="user-info">
                <div class="banner gs-row">
                    <div class="avatar-container">
                        <div class="user-tag">
                            <h1 class="tag">USER</h1>
                        </div>
                        <div class="avatar">
                            <img src="<?= $this->theme(); ?>/_assets/img/user.png">
                        </div>
                    </div>
                    <div class="info col-7">
                        <header>
                            <h1 class="name">adrianxc</h1>
                            <span class="status">Offline</span>
                        </header>
                        <div class="content">
                            <form id="status-form" class="status-form form-inline" action="javascript:void(0);">
                                <div class="input-group">
                                <textarea readonly id="me-status" type="text" maxlength="100" rows="1" class="status readonly autosize"
                                          spellcheck="false" placeholder="Qual sua próxima missão?"></textarea>
                                    <a id="edit-status" class="edit-btn"><i class="hbbi hbbi-tools-edit"></i></a>
                                </div>
                            </form>
                            <div class="acc-info">
                                <div class="val-box" title="Moedas">
                                    <i class="icon hbbi hbbi-credit"></i>
                                    <span class="val">0</span>
                                </div>
                                <div class="val-box" title="Diamantes">
                                    <i class="icon hbbi hbbi-diamond"></i>
                                    <span class="val">0</span>
                                </div>
                                <div class="val-box" title="Duckets">
                                    <i class="icon hbbi hbbi-ducket"></i>
                                    <span class="val">0</span>
                                </div>
                            </div>
                            <div class="last-access">
                                <span>Hoje - 13:15 PM</span>
                            </div>
                        </div>
                    </div>
                    <div class="r-content col-5">
                        <a href="#" class="hotel-btn play-btn">
                            HOTEL
                            <i class="hbbi hbbi-hotel-flat"></i>
                        </a>
                    </div>
                </div>
            </section>

            <section id="ad" class="ad-container ad-leaderboard">
                <div class="ad">
                    <span class="name">ANÚNCIO</span>
                </div>
            </section>
        </div>
    </div>
    <!-- Footer -->
<?php $this->partial('footer'); ?>